function bannerSwitcher() {
    next = $('.sec-2-input').filter(":checked").next('.sec-2-input');
    if (next.length) next.prop('checked', true);
    else $('.sec-2-input').first().prop('checked', true);
  }

  var bannerTimer = setInterval(bannerSwitcher, 5000);

  $('nav .controls label').click(function() {
    clearInterval(bannerTimer);
    bannerTimer = setInterval(bannerSwitcher, 5000)
  });


  /*********************************************************************** */
  