<?php include ("db.php") ?>
<?php include("includes/heater.php") ?>

<div class="container p-3">
    <div class="col-md-12">
        <form id="formularioLogo">
            <a href="index.php"  >
                <img src="assets\img\testinglic.png" style="max-height: 150px; max-width: 150px;">
            </a>
        </form>
    </div>
</div>

<div class="container p-4" id="formularioAlumno">
    <div class="col-md-12">
        <form action="agregar.php" method="POST" >
            <p>Para empezar... escribe tus datos por favor.
            <hr color="white">
            <div class="form-row">
                <div class="col-md-3 mb-2">
                    <label >Nombre(s)</label>
                    <input type="text" name="nombre" class="form-control" id="borderInput">
                </div>
                <div class="col-md-3 mb-2">
                    <label >Apellido paterno</label>
                    <input type="text" name="paterno" class="form-control" id="borderInput">
                </div>
                <div class="col-md-3 mb-2">
                    <label >Apellido materno</label>
                    <input type="text" name="materno" class="form-control" id="borderInput">
                </div>
                <div class="col-md-3 mb-2">
                    <label >Edad</label>
                    <input type="text" name="edad" class="form-control" id="borderInput">
                </div>                    
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-2">
                    <div class="form-group">
                        <label >Sexo</label>
                        <select name="sexo"class="form-control" id="borderInput">
                            <option>Masculino</option>
                            <option>Femenino</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 mb-2">
                    <div class="form-group">
                        <label >Estado</label>
                        <select class="form-control" id="borderInput" name="Estado">
                            <option>Veracruz</option>
                            <option>Puebla</option>
                            <option>Oaxaca</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 mb-2">
                <div class="form-group">
                        <label >Municipio</label>
                        <select class="form-control" id="borderInput" name="municipio">
                            
                        </select>
                    </div>
                </div>
                <div class="col-md-3 mb-2">
                <div class="form-group">
                        <label >Preparatoria</label>
                        <select class="form-control" id="borderInput" name="preparatoria">
                            
                        </select>
                    </div>
                </div>                    
            </div>
            <div class="form-row">
                <div class="col-md-12 mb-2">
                    <div class="form-group">
                        <label >¿Sabes que carrera estudiar?</label>
                        <select class="form-control" id="borderInput" name="resp">
                            <option>Sí</option>
                            <option>No</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>  
            <form action="" id="formBoton">
                <input id="btn" type="button" class="btn  btn-block" name="agregar" value="Continuar">
            </form>
    </div>        
</div>
<?php include("includes/footer.php") ?>