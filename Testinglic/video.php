<?php include("db.php") ?>
<?php include("includes/heater1.php");

//RECUPERAR EL ID DEL ALUMNO
if (isset($_POST['mandar_id'])) {
  $idAlumno = $_POST['idAlumno'];

?> <script>
    alert("!Se recibieron datos! id = " + <?php echo $idAlumno; ?>);
  </script>
<?php

} else {
?>
  <script>
    alert("!No se recibieron datos!");
  </script>
<?php } ?>
<div class="container">
  <header class="blog-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
      <div class="col-4 pt-1">

      </div>
      <div class="col-4 text-center">
        <table>
          <tr>
            <td><img src="img\testinglic.png" style="max-height: 50px; max-width: 50px;"></td>
            <td><a class="blog-header-logo text-dark">TESTINGLIC</a></td>
          </tr>
        </table>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
      </div>
    </div>
  </header>

  <div class="row flex-nowrap justify-content-between align-items-center">
    <div class="col-4 pt-1">
      <a class="p-2 text-muted tooltip-viewport-bottom" title="Dos estudiantes que pasamos por una situación en donde no sabíamos con exactitud qué estudiar y que tuvimos que cambiar de carrera en segundo semestre.">¿Quiénes somos?</a>
    </div>
    <div class="col-4 text-center">
      <a class="p-2 text-muted tooltip-bottom" title="Un test de orientación.">¿Qué haremos?</a>
    </div>
    <div class="col-4 d-flex justify-content-end align-items-center">
      <a class="p-2 text-muted tooltip-bottom" title="Ayudar a los alumos de nuevo ingreso para una selección adecuada de su carrera profesional.">¿Qué lograremos?</a>
    </div>
  </div>

  <div class="jumbotron p-3 p-md-5 text-black rounded bg-red" style="background-color: rgb(13,123,148)">
    <div class="col-md-12 px-0">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe src="videos\prueba.mp4" allowfullscreen></iframe>
      </div>
    </div>
  </div>

  <div class="pregresp2 row align-items-center">
    <div class="col-4">
      <form action="test.php" method="POST" autocomplete="off">
        <button class="btn btn-color btn-block" type="submit" name="mandar_id" value="<?php $id; ?>">Continuar</button>
    </div>
    <div class="col-4 form-group"><br>
      <h4 class="text-right">Número de registro:</h4>
    </div>
    <div class="col-4 form-group"><br>

      <input type="text" name="idAlumno" class="form-control text-center" value="<?= htmlspecialchars($idAlumno); ?>" readonly />
      </form>
    </div>

  </div>
</div>
<?php include("includes/footer1.php") ?>