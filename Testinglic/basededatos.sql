CREATE TABLE Preparatoria
(
  numero VARCHAR(200) NOT NULL,
  idPreparatoria VARCHAR(200) NOT NULL,
  Nombre VARCHAR(200) NOT NULL,
  Tipo VARCHAR(200) NOT NULL,
  Control VARCHAR(200) NOT NULL,
  clvestado VARCHAR(200) NOT NULL,
  Estado VARCHAR(200) NOT NULL,
  Municipio VARCHAR(200) NOT NULL,
  Localidad VARCHAR(200) NOT NULL,
  PRIMARY KEY (idPreparatoria)
)
ENGINE = InnoDB;


CREATE TABLE Alumnos
(
  idAlumno INT NOT NULL
  AUTO_INCREMENT,
  nombre VARCHAR
  (200) NOT NULL,
  apaterno VARCHAR
  (200) NOT NULL,
  amaterno VARCHAR
  (200) NOT NULL,
  edad VARCHAR
  (200) NOT NULL,
  sexo VARCHAR
  (200) NOT NULL,
  fechaRegis VARCHAR
  (200) NOT NULL,
  horaRegis VARCHAR
  (200) NOT NULL,
  respCarrera VARCHAR
  (200) NOT NULL,
  Preparatoria_idPreparatoria VARCHAR
  (200) NOT NULL,
  PRIMARY KEY
  (idAlumno, Preparatoria_idPreparatoria),
  INDEX fk_Alumnos_Preparatoria_idx
  (Preparatoria_idPreparatoria ASC),
  CONSTRAINT fk_Alumnos_Preparatoria
    FOREIGN KEY
  (Preparatoria_idPreparatoria)
    REFERENCES Preparatoria
  (idPreparatoria)
    ON
  DELETE CASCADE
    ON
  UPDATE CASCADE)
ENGINE = InnoDB;

  CREATE TABLE Resultados
  (
    resultado VARCHAR(100) NOT NULL,
    alumnos_idAlumno INT(11) NOT NULL,
    PRIMARY KEY (alumnos_idAlumno),
    CONSTRAINT fk_resultados_alumnos
    FOREIGN KEY (alumnos_idAlumno)
    REFERENCES Alumnos (idAlumno)
    ON DELETE CASCADE
    ON UPDATE CASCADE
  )
  ENGINE = InnoDB;