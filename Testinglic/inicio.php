<?php
include("db.php");
//RECIBE EL ESTADO DEL QUE SE VISITA EL ALUMNO Y SE LLENA EL FORMULARIO
include("includes/heater.php");
$estadocunsulta = 0;
if (isset($_POST['mandar_estado'])) {
    $estado = $_POST['estado'];
    $estadocunsulta = $estado;
}
?>

<form class="form-registro-alumno" action="principal.php" method="POST" autocomplete="off">
    <div class="text-center mb-4">
        <img class="" src="img\testinglic.png" alt="" width="145" height="145">
        <p class="text-left">Para empezar... escribe tus datos por favor.</p>
        <hr color=rgb(255,87,34)>
    </div>
    <div class="form-row">
        <div class="col-md-3 mb-2">
            <label>Nombre(s)</label>
            <input autocomplete="off" type="text" name="alumno" class="form-control" id="borderInput" required autofocus>
        </div>
        <div class="col-md-3 mb-2">
            <label>Apellido paterno</label>
            <input type="text" name="paterno" class="form-control" id="borderInput" autocomplete="off" required>
        </div>
        <div class="col-md-3 mb-2">
            <label>Apellido materno</label>
            <input type="text" name="materno" class="form-control" id="borderInput" autocomplete="off" required>
        </div>
        <div class="col-md-3 mb-2">
            <label>Edad</label>
            <input type="text" name="edad" class="form-control" id="borderInput" autocomplete="off" required>
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-4 mb-2">
            <div class="form-group">
                <label>Sexo</label>
                <select name="sexo" class="form-control" id="borderInput" required>
                    <option value="">Seleccione</option>
                    <option value="Masculino">Masculino</option>
                    <option value="Femenino">Femenino</option>
                </select>
            </div>
        </div>
        <div class="col-md-8 mb-2">
            <div class="form-group">
                <label>Preparatoria</label>
                <select class="form-control" id="borderInput" name="nombre" required>
                    <option value="0">Seleccione</option>
                    <?php
                    $query1 = "SELECT nombre,idPreparatoria FROM Preparatoria where clvestado=$estado ORDER BY `nombre` ASC";
                    $rec = mysqli_query($connect, $query1);
                    var_dump($rec);
                    while ($row = mysqli_fetch_array($rec)) {
                        $nombre = $row['nombre'];
                        $id = $row['idPreparatoria'];
                    ?>
                        <option value="<?php echo $id; ?>"><?php echo $nombre; ?></option>";
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-12 mb-2">
            <div class="form-group">
                <label>¿Sabes que carrera estudiar?</label>
                <select class="form-control" id="borderInput" name="resp" required>
                    <option value="">Seleccione</option>
                    <option value="Si">Sí</option>
                    <option value="No">No</option>
                </select>
            </div>
        </div>
    </div>
    <hr color=rgb(255,87,34)>
    <button class="btn btn-color btn-block" type="submit" name="registrar_alumno">Continuar</button>
    <p class="mt-5 mb-3 text-muted text-center">Instituto Tecnológico de Tehuacán</p>
</form>
<?php include("includes/footer.php") ?>