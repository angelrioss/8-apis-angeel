<?php include("db.php") ?>
<?php include("includes/heater1.php");

//RECUPERAR EL ID DEL ALUMNO
if (isset($_POST['mandar_id'])) {
  $idAlumno = $_POST['idAlumno'];

?> <script>
    alert("!Se recibieron datos! id = " + <?php echo $idAlumno; ?>);
  </script>
<?php

} else {
?>
  <script>
    alert("!No se recibieron datos!");
  </script>
<?php } ?>

<div class="container">
  <header class="blog-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
      <div class="col-4 pt-1">

      </div>
      <div class="col-4 text-center">
        <table>
          <tr>
            <td><img src="img\testinglic.png" style="max-height: 50px; max-width: 50px;"></td>
            <td><a class="blog-header-logo text-dark">TESTINGLIC</a></td>
          </tr>
        </table>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
      </div>
    </div>
  </header>


  <div class="jumbotron p-3 p-md-5 text-black rounded bg-red" style="background-color: white;">
    <div class="col-md-12 px-0">
      <div class="pregresp2">
        <div class="text-justify pregunta">Este es un test de orientación para Ingeniería en Sistemas Computacionales. Responde a todas las preguntas y da clic en "Continuar" para saber el resultado.</div>
        <br>
      </div>
      <form action="resultados.php" method="POST" autocomplete="off">
        <div class="pregresp">
          <div class="text-justify pregunta">1. ¿Te gusta la tecnología?</div>
          <select class="form-control respuestas" id="borderInput" name="pregunta1" required>
            <option value="">Seleccione</option>
            <option value="1">Sí</option>
            <option value="2">No</option>
            <option value="3">No</option>
          </select>
        </div>
        <div class="pregresp">
          <div class="text-justify pregunta">2. ¿Te adaptas rapidamente a los lenguajes de programación?<br /></div>
          <select class="form-control respuestas" id="borderInput" name="pregunta2" required>
            <option value="">Seleccione</option>
            <option value="1">Sí</option>
            <option value="2">No</option>
            <option value="3">No</option>
          </select>
        </div>
        <div class="pregresp">
          <div class="text-justify pregunta">3. ¿Se te difucultan las matemáticas?<br /></div>
          <select class="form-control respuestas" id="borderInput" name="pregunta3" required>
            <option value="">Seleccione</option>
            <option value="1">Sí</option>
            <option value="2">No</option>
            <option value="3">No</option>
          </select>
        </div>
        <div class="pregresp2">
          <br>
          <div class="text-justify pregunta">Si has llegado hast aquí, significa que es el momento de saber el resultado de tu Test de orientación para Ingeniería en Sistemas Computacionales. Para conocer cual fue tu resultado da clic en el siguiente botón:<br /></div>
        </div>
        <div class="pregresp2 row align-items-center">
          <div class="col-4">
            <button class="btn btn-color btn-block" type="submit" name="mandar_respuestas" value="<?php $id; ?>">Continuar</button>
          </div>
          <div class="col-4 form-group"><br>
            <h4 class="text-right">Número de registro:</h4>
          </div>
          <div class="col-4 form-group"><br>
            <input type="text" name="idAlumno" class="form-control text-center" value="<?= htmlspecialchars($idAlumno); ?>" readonly />
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<?php include("includes/footer1.php") ?>