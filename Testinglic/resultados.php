<?php include("db.php") ?>
<?php include("includes/heater1.php");

//RECUPERAR EL ID DEL ALUMNO
if (isset($_POST['mandar_respuestas'])) {
    $idAlumno = $_POST['idAlumno'];
    $preg1 = $_POST['pregunta1'];
    $preg2 = $_POST['pregunta2'];
    $preg3 = $_POST['pregunta3'];
?> <script>
        alert("!Se recibieron datos! id = " + <?php echo $idAlumno; ?>);
    </script>
<?php
    $resultado = $preg1 + $preg2 + $preg3;
} else {
?>
    <script>
        alert("!No se recibieron datos!");
    </script>
<?php } ?>

<div class="container">
    <header class="blog-header py-3">
        <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="col-4 pt-1">

            </div>
            <div class="col-4 text-center">
                <table>
                    <tr>
                        <td><img src="img\testinglic.png" style="max-height: 50px; max-width: 50px;"></td>
                        <td><a class="blog-header-logo text-dark">TESTINGLIC</a></td>
                    </tr>
                </table>
            </div>
            <div class="col-4 d-flex justify-content-end align-items-center">
            </div>
        </div>
    </header>


    <div class="jumbotron p-3 p-md-5 text-black rounded bg-red" style="background-color: white;">
        <div class="col-md-12 px-0">
            <div class="pregresp2">
                <div class="text-justify pregunta">Este es un test de orientación para Ingeniería en Sistemas Computacionales. Responde a todas las preguntas y da clic en "Continuar" para saber el resultado.</div>
                <br>
            </div>
            <form action="video.php" method="POST" autocomplete="off">
                <div class="pregresp">
                    <div class="text-justify pregunta">1. ¿Te gusta la tecnología?<br /></div>
                    <div class="respuestas">
                        <input type="radio" name="p1" value="1" /> Ah bueno pa saber...<br />
                        <input type="radio" name="p1" value="2" /> Ah bueno te me cuidas<br />
                        <input type="radio" name="p1" value="3" /> Oh oc<br />
                    </div>
                </div>
                <div class="pregresp">
                    <div class="text-justify pregunta">2. ¿Te adaptas rapidamente a los lenguajes de programación?<br /></div>
                    <div class="respuestas">
                        <input type="radio" name="p2" value="1" /> Ah bueno pa saber...<br />
                        <input type="radio" name="p2" value="2" /> Ah bueno te me cuidas<br />
                        <input type="radio" name="p2" value="3" /> Oh oc<br />
                    </div>
                </div>
                <div class="pregresp">
                    <div class="text-justify pregunta">3. ¿Se te difucultan las matemáticas?<br /></div>
                    <div class="respuestas">
                        <input type="radio" name="p3" value="1" /> Ah bueno pa saber...<br />
                        <input type="radio" name="p3" value="2" /> Ah bueno te me cuidas<br />
                        <input type="radio" name="p3" value="3" /> Oh oc<br />
                    </div>
                </div>
                <div class="pregresp2">
                    <br>
                    <div class="text-justify pregunta">Si has llegado hast aquí, significa que es el momento de saber el resultado de tu Test de orientación para Ingeniería en Sistemas Computacionales. Para conocer cual fue tu resultado da clic en el siguiente botón:<br /></div>
                </div>
                <div class="pregresp2 row align-items-center">
                    <div class="col-4">
                        <button class="btn btn-color btn-block" type="submit" name="mandar_respuestas" value="<?php $id; ?>">Continuar</button>
                    </div>
                    <div class="col-4 form-group"><br>
                        <h4 class="text-right">Número de registro:</h4>
                    </div>
                    <div class="col-4 form-group"><br>
                        <input type="text" name="idAlumno" class="form-control text-center" value="<?= htmlspecialchars($idAlumno); ?>" readonly />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include("includes/footer1.php") ?>