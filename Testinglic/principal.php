<?php include("db.php") ?>
<?php include("includes/heater1.php");
echo $id = 20;
//REGISTRAR ALUMNO CON LOS DATOS DEL FORMULARIO
if (isset($_POST['registrar_alumno'])) {
  $alumnonombre = $_POST['alumno'];
  $paterno = $_POST['paterno'];
  $materno = $_POST['materno'];
  $edad = $_POST['edad'];
  $sexo = $_POST['sexo'];
  $preparatoria = $_POST['nombre'];
  $respcarrera = $_POST['resp'];

  ini_set('date.timezone', 'America/mexico_City');
  $hora = date('H:i:s', time());
  $fecha = date('d-m-Y', time());


  //INSERT A LA TABLA DE ALUMNOS
  $query = "INSERT INTO Alumnos(nombre,apaterno,amaterno,edad,sexo,fechaRegis,horaRegis,respCarrera,Preparatoria_idPreparatoria) VALUES ('$alumnonombre','$paterno','$materno','$edad','$sexo','$fecha','$hora','$respcarrera','$preparatoria')";
  $result = mysqli_query($connect, $query); //EJECUTA CONSULTA

  /*$query1 = "SELECT idAlumno from Alumnos WHERE nombre='$alumnonombre' and apaterno='$paterno' AND amaterno='$materno' and edad='$edad' and sexo='$sexo' and respCarrera='$respcarrera' and Preparatoria_idPreparatoria='$preparatoria' and fechaRegis='$fecha' and horaRegis='$hora'";
  $rec = mysqli_query($connect, $query1) or die("Error en la Consulta SQL");
  while ($row = mysqli_fetch_array($rec)) {
    $id = $row['idAlumno'];
  }*/

?> <script>
    alert("!Se recibieron datos! id = " + <?php echo $id; ?>);
  </script>
<?php

} else {
?>
  <script>
    alert("!No se recibieron datos!");
  </script>
<?php } ?>

<script>
  alert("!Se recibieron datos! id = " + <?php echo $id; ?>);
</script>


<div class="container">
  <header class="blog-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
      <div class="col-4 pt-1">

      </div>
      <div class="col-4 text-center">
        <table>
          <tr>
            <td><img src="img\testinglic.png" style="max-height: 50px; max-width: 50px;"></td>
            <td><a class="blog-header-logo text-dark">TESTINGLIC</a></td>
          </tr>
        </table>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
      </div>
    </div>
  </header>

  <div class="row flex-nowrap justify-content-between align-items-center">
    <div class="col-4 pt-1">
      <a class="p-2 text-muted tooltip-viewport-bottom" title="Dos estudiantes que pasamos por una situación en donde no sabíamos con exactitud qué estudiar y que tuvimos que cambiar de carrera en segundo semestre.">¿Quiénes somos?</a>
    </div>
    <div class="col-4 text-center">
      <a class="p-2 text-muted tooltip-bottom" title="Un test de orientación.">¿Qué haremos?</a>
    </div>
    <div class="col-4 d-flex justify-content-end align-items-center">
      <a class="p-2 text-muted tooltip-bottom" title="Ayudar a los alumos de nuevo ingreso para una selección adecuada de su carrera profesional.">¿Qué lograremos?</a>
    </div>
  </div>

  <div class="jumbotron p-3 p-md-5 text-black rounded bg-red">
    <div class="col-md-6 px-0">
      <h1 class="display-4 font-italic">¿No sabes qué estudiar?</h1>
      <p class="lead my-3 text-justify">De acuerdo con la <a href="#">ANUES</a>, en México de cada 100 estudiantes que ingresan a las Instituciones de Educación Superior, sólo 60 egresan y de éstos, sólo 20 se titulan.</p>
    </div>
  </div>

  <div class="row mb-2">
    <div class="col-md-6">
      <div class="card flex-md-row mb-4 box-shadow h-md-310">
        <div class="card-body d-flex flex-column align-items-start">
          <h3 class="mb-0">
            <a class="text-dark">Problemática:</a>
          </h3>
          <p class="card-text mb-auto text-justify">El Instituto Tecnológico de Tehuacán enfrenta un grave problema con los alumnos que ingresan al plantel sin previo conocimiento de lo que trata la carrera que han elegido, que en muchos de los casos provocan una alarmante situación de deserción de la misma; son pocos los que en realidad conocen sus intereses vocacionales, habilidades y conocimientos.</p>
        </div>
        <img class="card-img-right flex-auto d-none d-md-block" src="img\problematica.png" alt="Card image cap" style="max-height: 300px; max-width: 350px;">
      </div>
    </div>
    <div class="col-md-6">
      <div class="card flex-md-row mb-4 box-shadow h-md-315">
        <div class="card-body d-flex flex-column align-items-start">
          <h3 class="mb-0">
            <a class="text-dark">Objetivo general:</a>
          </h3>
          <p class="card-text mb-auto text-justify">Implementar un sitio web utilizando las diferentes herramientas de desarrollo para orientar a estudiantes de nivel medio superior que deseen ingresar al área de ingeniería en sistemas computacionales para poder así facilitar la selección adecuada de su carrera profesional.</p>
        </div>
        <img class="card-img-right flex-auto d-none d-md-block" src="img\objetivo.png" alt="Card image cap" style="max-height: 300px; max-width: 350px;">
      </div>
    </div>
  </div>
</div>

<main role="main" class="container">
  <div class="row">
    <div class="col-md-8 blog-main">

      <div class="blog-post">
        <h2 class="blog-post-title">Propuesta de proyecto</h2>
        <p class="text-justify">Consiste en realizar una página web, en esta daremos un tipo de asesoramiento para alumnos de educación media superior que ayude a la selección adecuada de su <strong>futuro profesional</strong>. Esto será en base a un test; dependiendo de los resultados que arroje el sistema abrirá la opción para poder visualizar un corto video acerca de las 3 carreras más afines al alumno en donde se le explique de manera precisa y corta de lo que trata la carrera y del por qué el <strong>Instituto Tecnológico de Tehuacán</strong> es su mejor opción.</p>
        <p class="text-justify">De momento y como parte de la primera fase solo fue desarrollado para el área de <strong>Ing. en Sistemas Computacionales</strong>.</p>
      </div><!-- /.blog-post -->

    </div><!-- /.blog-main -->

    <aside class="col-md-4 blog-sidebar">
      <div class="p-5">
        <h4 class="font-italic text-center">¿Estás listo?</h4>
        <ol class="list-unstyled">
          <form action="video.php" method="POST" autocomplete="off">
            <p>Numero de registro:</p>
            <input type="text" name="idAlumno" class="form-control text-center" id="borderInput" value="<?= htmlspecialchars($id); ?>" size="8" readonly />
            <br>
            <button class="btn btn-color btn-block" type="submit" name="mandar_id" value="<?php $id; ?>">Continuar</button>
          </form>
        </ol>
      </div>
    </aside><!-- /.blog-sidebar -->

  </div><!-- /.row -->

</main><!-- /.container -->


<?php include("includes/footer1.php") ?>