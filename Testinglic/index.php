<?php include("db.php") ?>
<?php include("includes/heater.php") ?>
<form class="form-estado" action="inicio.php" method="POST">
  <div class="text-center mb-4">
    <img class="mb-4" src="img\testinglic.png" alt="" width="150" height="150">
    <h2 class="h3 mb-3 font-weight-normal">¿De qué estado nos visitas?</h2>
    <p>Selecciona uno por favor</p>
  </div>

  <div class="form-label-group">
    <select class="form-control" id="borderInput" name="estado" required style="border-radius: 10px;">
      <option value="">Seleccione</option>
      <option value="30">Veracruz</option>
      <option value="21">Puebla</option>
      <option value="20">Oaxaca</option>
    </select>
  </div>
  <button class="btn btn-color btn-block" type="submit" name="mandar_estado">Continuar</button>
  <p class="mt-5 mb-3 text-muted text-center">Instituto Tecnológico de Tehuacán</p>
</form>
<?php include("includes/footer.php") ?>